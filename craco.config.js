const CracoLessPlugin = require("craco-less");
const { Theme } = require("./src/theme");

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              "@primary-color": Theme.PrimaryColor,
              "@link-color": Theme.LinkColor,
              "@success-color": Theme.SuccessColor,
              "@warning-color": Theme.WarningColor,
              "@error-color": Theme.ErrorColor,
              "@font-size-base": Theme.FontSizeBase,
              "@heading-color": Theme.HeadingColor,
              "@text-color": Theme.TextColor,
              "@text-color-secondary": Theme.TextColorSecondary,
              "@disabled-color": Theme.DisabledColor,
              "@border-radius-base": Theme.BorderRadiusBase,
              "@border-color-base": Theme.BorderColorBase,
              "@box-shadow-base": Theme.BoxShadowBase,
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
