import { createAsyncThunk } from "@reduxjs/toolkit";
import AgenciesAPIs from "../../api/agencies";
import { IAgency } from "../../models/agencies";
import { IPaginatedResult, IThunkAPIConfig } from "../../models/general";

const Search = createAsyncThunk<
  IPaginatedResult<IAgency>,
  string,
  IThunkAPIConfig
>("agencies/search", async (search, { rejectWithValue }) => {
  try {
    const agenciesRequest = await AgenciesAPIs.Search(search);
    return agenciesRequest.data;
  } catch (e: any) {
    return rejectWithValue({ errorMessage: e.toString() });
  }
});
const AgenciesAsync = { Search };
export default AgenciesAsync;
