import { createSlice } from "@reduxjs/toolkit";
import AgenciesAsync from "./async";
import { IAgenciesState } from "./types";

const initialState: IAgenciesState = {
  list: [],
  loading: false,
  error: "",
  count: 0,
  filters: { search: "" },
};
const { Search } = AgenciesAsync;
export const agenciesSlice = createSlice({
  initialState,
  name: "agencies",
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(Search.fulfilled, (state, action) => {
      state.list = action.payload.results;
      state.count = action.payload.count;
      state.loading = false;
      state.error = "";
    });
    builder.addCase(Search.pending, (state, action) => {
      state.loading = true;
      state.error = "";
      state.filters = { search: action.meta.arg };
    });
    builder.addCase(Search.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload?.errorMessage;
    });
  },
});
export default agenciesSlice.reducer;
