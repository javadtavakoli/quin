import { IAgency } from "../../models/agencies";

export interface IAgenciesState {
  filters: { search: string };
  list: IAgency[];
  count: number;
  loading: boolean;
  error?: string;
}
