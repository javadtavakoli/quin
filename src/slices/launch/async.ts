import { createAsyncThunk } from "@reduxjs/toolkit";
import LaunchAPIs from "../../api/launch";
import { IPaginatedResult, IThunkAPIConfig } from "../../models/general";
import { ILaunch, ILaunchFilters } from "../../models/launch";

const Filter = createAsyncThunk<
  IPaginatedResult<ILaunch>,
  ILaunchFilters,
  IThunkAPIConfig
>("launches/search", async (search, { rejectWithValue }) => {
  try {
    const averagesRequest = await LaunchAPIs.Filter(search);
    return averagesRequest.data;
  } catch (e: any) {
    return rejectWithValue({ errorMessage: e.toString() });
  }
});
const LaunchesAsync = { Filter };
export default LaunchesAsync;
