import { createSlice } from "@reduxjs/toolkit";
import LaunchesAsync from "./async";
import { ILaunchState } from "./types";

const initialState: ILaunchState = {
  list: [],
  loading: false,
  error: "",
  count: 0,
  filters: {  },
};
const { Filter } = LaunchesAsync;
export const launchesSlice = createSlice({
  initialState,
  name: "launches",
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(Filter.fulfilled, (state, action) => {
      state.list = action.payload.results;
      state.count = action.payload.count;
      state.loading = false;
      state.error = "";
    });
    builder.addCase(Filter.pending, (state, action) => {
      state.loading = true;
      state.error = "";
      state.filters = action.meta.arg;
    });
    builder.addCase(Filter.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload?.errorMessage;
    });
  },
});
export default launchesSlice.reducer;
