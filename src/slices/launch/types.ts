import { ILaunch, ILaunchFilters } from "../../models/launch";

export interface ILaunchState {
  filters: ILaunchFilters;
  list: ILaunch[];
  count: number;
  loading: boolean;
  error?: string;
}
