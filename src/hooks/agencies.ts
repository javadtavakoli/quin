import { useEffect } from "react";
import AgenciesAsync from "../slices/agencies/async";
import { useAppDispatch } from "./redux";

const useAgencies = () => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    search("");
  }, []);
  const search = (search: string) => {
    dispatch(AgenciesAsync.Search(search));
  };
  return { search };
};
export default useAgencies;
