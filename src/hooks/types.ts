import { Moment } from "moment";

export interface IFilterLaunchesFormValues {
  date?: [Moment, Moment];
  agencies?: number[];
  stauts?: "Success" | "Failure";
}
