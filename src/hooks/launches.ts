import LaunchesAsync from "../slices/launch/async";
import { useAppDispatch } from "./redux";
import { IFilterLaunchesFormValues } from "./types";

const useLaunches = () => {
  const dispatch = useAppDispatch();

  const filter = (filters: IFilterLaunchesFormValues) => {
    dispatch(
      LaunchesAsync.Filter({
        lsp__ids: filters.agencies,
        status: filters.stauts,
        window_start__gt: filters.date?.[0].utc().toISOString(),
        window_end__lt: filters.date?.[1].utc().toISOString(),
      })
    );
  };
  return { filter };
};
export default useLaunches;
