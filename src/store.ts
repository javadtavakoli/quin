import { Action, configureStore, ThunkAction } from "@reduxjs/toolkit";
import agenciesReducer from "./slices/agencies";
import launchesReducer from "./slices/launch";
export const store = configureStore({
  reducer: {
    agencies: agenciesReducer,
    launches: launchesReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
