import { message } from "antd";
import axios from "axios";
const baseURL =
  process.env.NODE_ENV === "test" ? "" : process.env.REACT_APP_BASE_URL;
const Axios = axios.create({ baseURL });
Axios.interceptors.response.use(
  (res) => res,
  (err) => {
    if (err.response.status === 404) {
      message.error("Not Found");
    }
    if (err.response && err.response.data && err.response.data.code) {
      message.error(err.response.data.message);
    } else {
      message.error("Unknown Error");
    }
    return Promise.reject(err);
  }
);
export default Axios;
