export interface IPaginatedResult<ItemType> {
  count: number;
  results: ItemType[];
  next?: string;
  previous?: string;
}
export interface IThunkAPIConfig {
    rejectValue: {
      errorMessage: string;
    };
  }
  