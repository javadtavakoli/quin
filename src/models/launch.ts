export interface ILaunchFilters {
  window_start__gt?: string;
  window_end__lt?: string;
  lsp__ids?: number[];
  status?: "Success" | "Failure";
}
export interface ILaunch {
  name: string;
  pad: {
    name: string;
    latitude: string;
    longitude: string;
    window_end: Date;
  };
  launch_service_provider: {
    name: string;
  };
}
