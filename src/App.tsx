import { Layout } from "antd";
import React from "react";
import AppStyled from "./styles/app";
import "antd/dist/antd.less";
import FilterForm from "./components/filterForm";
import LaunchMap from "./components/launchMap";
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';

delete (L.Icon.Default.prototype as any)._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});
function App() {
  const { Content, Card } = AppStyled;
  return (
    <Layout>
      <Content>
        <Card title="Moonshot Calendar Inc.">
          <FilterForm />
          <LaunchMap />
        </Card>
      </Content>
    </Layout>
  );
}

export default App;
