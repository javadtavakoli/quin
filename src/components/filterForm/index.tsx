import { Button, Col, DatePicker, Form, Row, Select } from "antd";
import Card from "antd/lib/card/Card";
import React from "react";
import { FilterOutlined } from "@ant-design/icons";
import useAgencies from "../../hooks/agencies";
import { useAppSelector } from "../../hooks/redux";
import useLaunches from "../../hooks/launches";

const FilterForm = () => {
  const { RangePicker } = DatePicker;
  const agencies = useAgencies();
  const agenciesState = useAppSelector((state) => state.agencies);
  const { filter } = useLaunches();
  const launchesState = useAppSelector((state) => state.launches);
  return (
    <Card type="inner" title="Filter Launch Results">
      <Form onFinish={filter}>
        <Row gutter={16}>
          <Col span={6}>
            <Form.Item label="Date" name="range">
              <RangePicker />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item label="Agencies" name="agencies">
              <Select
                mode="multiple"
                loading={agenciesState.loading}
                onSearch={agencies.search}
                filterOption={false}
              >
                {agenciesState.list.map((agency) => (
                  <Select.Option key={agency.id} value={agency.id}>
                    {agency.name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item label="Status" name="status">
              <Select>
                <Select.Option value="Success">Success</Select.Option>
                <Select.Option value="Failure">Failure</Select.Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={2} offset={1}>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                loading={launchesState.loading}
              >
                <FilterOutlined />
                Filter
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};
export default FilterForm;
