import { Card } from "antd";
import { LatLngExpression } from "leaflet";
import moment from "moment";
import React from "react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { useAppSelector } from "../../hooks/redux";
import LaunchMapStyled from "./styled";


const LaunchMap = () => {
  const { Container } = LaunchMapStyled;
  const { list } = useAppSelector((state) => state.launches);
  
  return (
    <Card type="inner" title="Map">
      <Container>
        <MapContainer
          center={[10,10]}
          style={{ height: "30em" }}
          zoom={2}
          scrollWheelZoom={false}
        >
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {list.map((launch) => (
            <Marker position={[+launch.pad.latitude, +launch.pad.longitude]}>
              <Popup>
                <p>
                  <strong>Name: </strong> {launch.name}
                </p>
                <p>
                  <strong>Time of launch: </strong>{" "}
                  {moment(launch.pad.window_end).format("lll")}
                </p>
                <p>
                  <strong>Name of launch pad: </strong> {launch.pad.name}
                </p>
                <p>
                  <strong>Agency: </strong>{" "}
                  {launch.launch_service_provider.name}
                </p>
              </Popup>
            </Marker>
          ))}
        </MapContainer>
      </Container>
    </Card>
  );
};
export default LaunchMap;
