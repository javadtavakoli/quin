interface ThemeType {
  PrimaryColor: string;
  LinkColor: string;
  SuccessColor: string;
  WarningColor: string;
  ErrorColor: string;
  FontSizeBase: string;
  HeadingColor: string;
  TextColor: string;
  TextColorSecondary: string;
  DisabledColor: string;
  BorderRadiusBase: string;
  BorderColorBase: string;
  BoxShadowBase: string;
  SecondaryColor: string;
}
export const Theme:ThemeType;