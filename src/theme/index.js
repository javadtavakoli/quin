const PrimaryColor = "#00aeef";
const SecondaryColor = "#efefef"
const LinkColor = "#00aeef";
const SuccessColor = "#52c41a";
const WarningColor = "#faad14";
const ErrorColor = "#f5222d";
const FontSizeBase = "16px";
const HeadingColor = "rgba(0, 0, 0, 0.85)";
const TextColor = "rgba(0, 0, 0, 0.87)";
const TextColorSecondary = "#757779";
const DisabledColor = "#d5d5d5";
const BorderRadiusBase = "5px";
const BorderColorBase = "#ddedf9";
const BoxShadowBase = "0 2px 6px 0 rgba(0, 0, 0, 0.12)";


const Theme = {
  PrimaryColor,
  SecondaryColor,
  LinkColor,
  SuccessColor,
  WarningColor,
  ErrorColor,
  FontSizeBase,
  HeadingColor,
  TextColor,
  TextColorSecondary,
  DisabledColor,
  BorderRadiusBase,
  BorderColorBase,
  BoxShadowBase,
};


module.exports = { Theme: Theme };
