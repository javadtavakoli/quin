import { AxiosResponse } from "axios";
import Axios from "../config/axios";
import { IPaginatedResult } from "../models/general";
import { ILaunch, ILaunchFilters } from "../models/launch";
const launchPerfix = "launch";
const Filter = (
  filters: ILaunchFilters
): Promise<AxiosResponse<IPaginatedResult<ILaunch>>> =>
  Axios.get(`${launchPerfix}`, { params: { filters } });
const LaunchAPIs = { Filter };
export default LaunchAPIs;
