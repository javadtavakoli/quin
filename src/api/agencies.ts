import { AxiosResponse } from "axios";
import Axios from "../config/axios";
import { IAgency } from "../models/agencies";
import { IPaginatedResult } from "../models/general";
const agenciesPrefix = "agencies";
const Search = (
  search: string
): Promise<AxiosResponse<IPaginatedResult<IAgency>>> =>
  Axios.get(`${agenciesPrefix}`, { params: { search } });
const AgenciesAPIs = { Search };
export default AgenciesAPIs;
