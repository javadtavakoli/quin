import { Card as AntdCard, Layout } from "antd";
import styled from "styled-components";

const { Content: AntdContent } = Layout;

const Content = styled(AntdContent)`
  padding: 0 4em;
  margin-top: 6em;
`;
const Card = styled(AntdCard)`
  .ant-card:not(:first-child) {
    margin-top: 1em;
  }
`;
const AppStyled = {
  Content,
  Card,
};
export default AppStyled;
